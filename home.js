const autocannon = require("autocannon");
require("dotenv").config();

function startBench() {
  const url = `${process.env.URI_STAGING}/api/v2/mobile/home`

  const args = process.argv.slice(2);
  const numConnections = args[0] || 100;
  //const maxConnectionRequests = args[1] || 100;

  const instance = autocannon(
    {
      url,
      connections: numConnections,
      duration: 10,
      //maxConnectionRequests,
      headers: {
        "content-type": "application/json",
      },
      requests: [
        {
          method: "GET",
          path: "/",
        },
      ],
    },
    finishedBench
  );

  autocannon.track(instance);

  function finishedBench(err, res) {
    console.log("Finished Bench", err, res);
  }
}

startBench();