require("dotenv").config();

let env = {
  uri: process.env.URI_STAGING,
  token: process.env.TOKEN_STAGING,
};

if (process.env.NODE_ENV === "local") {
  env = {
    uri: process.env.URI_LOCAL,
    token: process.env.TOKEN_STAGING,
  };
}

exports.env = env;
