const autocannon = require("autocannon");
const data = require("./data.json");
const { env } = require("./config");

function startBench() {
  const url = `${env.uri}/api/v1/mobile/orders`;

  const args = process.argv.slice(2);
  const numConnections = args[0] || 100;
  const timeout = 20;
  //const maxConnectionRequests = args[1] || 100;

  const instance = autocannon(
    {
      url,
      connections: numConnections,
      duration: 1,
      timeout,
      headers: {
        "content-type": "application/json",
        Authorization: `Bidu ${env.token}`,
      },
      requests: [
        {
          method: "POST",
          body: JSON.stringify(data),
        },
      ],
    },
    finishedBench
  );

  autocannon.track(instance);

  function finishedBench(err, res) {
    console.log("Finished Bench", err, res);
  }
}

startBench();
