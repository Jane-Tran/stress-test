# stress-test BIDU

## Init project

```
git clone https://gitlab.com/Jane-Tran/stress-test.git

yarn/npm install
```

## Change environment

In file .env and change value variable NODE_ENV:

```
NODE_ENV=local
```

or

```
NODE_ENV=staging
```

## Run stress test API create order

```
node create-order.js [numConnections]
```

## Restore DB

#### 1. Select DB staging

```
> use fashion-video-app-stg
```

#### 2. Delete many group order

```
> db.grouporders.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')})
```

#### 3. Delete many order

```
> db.orders.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')})
```

#### 4. Delete many order

```
> db.orders.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')})
```

#### 5. Delete many order shippings

```
> db.ordershippings.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')})
```

#### 6. Delete many order items (attention: edit the date the order item was created)

```
> db.orderitems.deleteMany({product_id: ObjectId('616f9888c2387d0012e14978'), "variant._id": "61a6eb17349fc6001342ca15", created_at: {$gt: new Date('2021-12-19')}})
```

#### 7. Update product

```
> db.products.update({_id: ObjectId('616f9888c2387d0012e14978')}, {$set: {"quantity": 1000000} })
```

#### 8. Update variants

```
> db.variants.update({_id: ObjectId('61a6eb17349fc6001342ca15')}, {$set: {"quantity": 1000000} })
```

#### 9. Update voucher shop & system

```
> db.vouchers.update({_id: ObjectId('6184b45428ef2b00139f0023')}, {$set: {"available_quantity": 1000000} }, )
> db.vouchers.update({_id: ObjectId('61a9ebabe122d3801bf0347d')}, {$set: {"available_quantity": 1000000} }, )
```

### 10. One compound command

```
db.grouporders.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')});  db.orders.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')}); db.ordershippings.deleteMany({user_id: ObjectId('61bc3675826ac10019c18edc')}); db.products.update({_id: ObjectId('616f9888c2387d0012e14978')}, {$set: {"quantity": 1000000} });  db.variants.update({_id: ObjectId('61a6eb17349fc6001342ca15')}, {$set: {"quantity": 1000000} }); db.vouchers.update({_id: ObjectId('6184b45428ef2b00139f0023')}, {$set: {"available_quantity": 1000000} }, ); db.vouchers.update({_id: ObjectId('61a9ebabe122d3801bf0347d')}, {$set: {"available_quantity": 1000000} }, ); db.orderitems.deleteMany({product_id: ObjectId('616f9888c2387d0012e14978'), "variant._id": "61a6eb17349fc6001342ca15", created_at: {$gt: new Date('2021-12-19')}})
```
